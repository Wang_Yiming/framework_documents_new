## Nginx 概念, 高性能原理

### 95. Nginx在互联网架构中的作用

LVS是运维干的活. 负载均衡，Nginx，邮件系统部署，都是他们干的. 

Java架构师, 对LVS和Nginx有一定的了解, 初创期在阿里云, 腾讯云, 采购一整套的基础架构. 

**通过云平台上的LVS和Nginx做一点简单的配置**



### 96. Nginx配置文件包括几块



Nginx+Lua做很多复杂的操作, 把这个脚本提供给你的运维，运维可以代替你去进行比较稳妥的Lua脚本的部署，如果运维 团队比较给力，甚至可以把基于Nginx部署Lua脚本这个事情做成一个Web界面平台，你可以通过一个Web界面去部署你的Lua脚本



```lua
# worker进程
worker_processes 3;
user nobody nobody;	# 意思就是所有linux用户和用户组都可以启动nginx
error_log logs/error.log;	# nginx服务器运行的错误日志存放路径
pid	nginx.pid;	# nginx服务器运行时的pid文件存放路径

# 这里配置的是nginx和用户之间的网络连接的一些东西
events {
    user epoll;	# 配置网络通信的事件驱动模型，这里用epoll
	worker_connections 1024;	# 最大有多少个客户端跟nginx可以建立连接
}

http {
    # 下面两个定义了能识别的多媒体类型
	include	mime.types;
	default_type	application/octet-stream;
    
    # 配置使用senfile方式进行网络数据传输
	sendfile	on;
	
    # 连接超时时间
    keepalive_timeout	65;
    
    # 配置请求日志的格式,
	# 采集的是用户的一些行为日志, 浏览器上前端代码在用户做了一些操作的时候
	# 就会把行为日志写入到后台的Nginx上去, Nginx就可以把请求日志写在自己本地
	# 大数据系统, 就可以通过flume去Nginx服务器上去捞他的请求日志
	log_format	access.log	'$remote_addr-[$time_local]-"$request"-"$http_user_agent"';

	# server是虚拟主机的概念
	server {
        # 这个虚拟机主键监听的端口号和主机名
		listen	80; 
		server_name	localhost;
        
        # 下面是配置针对这个虚拟主机的请求日志的存放地址
		access_log	/user/local/nginx/log/access.log
		
        # 配置请求错误的界面
		error_page	404 /404.html
        
		# 每一个location就代表一种请求如何处理
		location /server1 {	
            # 收到/server1/user.html请求
			# 会到/server-html目录下，找你请求的/server1目录下的user.html页面
			root	/server-html;
			# 站点默认页面
			index	index.html;
		}

		# 每一个location就代表一种请求如何处理
		location / {
			root	html;
			index	index.html;
		}
        
        error_page	500 502 503 504 /50x.html
        
        location = /50x.html {
            root		html;
        }
	}
}
```





### 99. 同步异步, 阻塞非阻塞 的关系

- **同步异步**: client端发送消息是不是要等着
- **阻塞非阻塞**: server端接受处理消息需不需要等着

**异步非阻塞:** 客户端发出请求不等待响应, 服务端收到请求后非阻塞IO, IO不能立马完成就去干别的, 等IO搞定了再通知客户端.





### 102. 抗下互联网高并发的服务器架构模式

- **主进程+子进程**: netty
- **多线程**: 单个jvm里的service都是





### 103. Nginx的通信IO模型、模块化架构和并发架构总结

- **异步非阻塞**
- **多进程机制**
  - 主进程: 负责建立, 绑定, 关闭socket.
  - worker紫禁城: 会非阻塞的处理IO.

- **模块化架构** 支持扩展



### 104. IO多路复用事件驱动模型: select, poll, epoll:

线程拿到一个请求, 全部得后续执行阻塞式IO; **Nginx是非阻塞式IO, 把IO交给内核就做别的处理.** 

linux内核提供的[select、poll、epoll、kqueue都是做非阻塞IO的][select、poll、epoll之间的区别总结[整理\] - Rabbit_Dale - 博客园 (cnblogs.com)](https://www.cnblogs.com/Anker/p/3265058.html)

- select, 针对关注的事件创建描述符集合, 包括read、write和exception三种事件描述符集合. 

  然后调用linux内核的select()函数, 轮询三个集合里的事件描述符做处理

- poll, 也是先创建事件描述符集合, 然后去轮询事件描述符集合

  **select要创建三个集合，poll只要创建一个集合**

- epoll, 是完全交给内核去做, 调用内核接口创建一个有N个描述符的事件列表, 给描述符设置关注的事件, 接着完全是内核自己轮询和管理, **有事件发生就回调通知进程**

**一般都是用epoll**



![Nginx架构原理](5.%20%E4%B8%B0%E5%AF%8C%E8%B4%9F%E8%BD%BD%E5%9D%87%E8%A1%A1%E5%B1%82-Nginx-95-105.assets/Nginx%E6%9E%B6%E6%9E%84%E5%8E%9F%E7%90%86.png)





## Nginx丰富实用: 四个功能

**Nginx两个应用:** 

1. **读取本地静态文件.**
2. **域名跳转**
3. **反向代理**
4. **负载均衡**

### 106. Rewrite功能实现域名跳转

```lua
sever {
	listen 80;

    # 如果请求的header里的·Referer·的server域名是这个, 就给它改写.
    # 直接做一个域名跳转，最终请求是http://www.zhss.com/去反馈的
    server_name	view.website.zhss;
	rewrite	^/ http://www.zhss.com/;
}
```



### 107. Rewrite功能实现域名跳转 和 域名镜像

域名镜像，多个站点互为主备

```java
server {
	listen 80;
	server_name view1.website.zhss view2.website.zhss;
	if ($http_host ~* ^(.*)\.website\.zhss$) {
		rewrite ^(.*) http://view.webite.zhss$1;
		break;
	}
}
请求http://view1.website.zhss/order，http://view2.website.zhss/order会跳转给http://view.website.zhss/order
```



### 108. 域名跳转/域名镜像的效果:  分类, 主备

- 怎么知道request请求的是哪个域名?  http里面的有个`refere`字段.



### 109. Rewrite实现 独立域名 和 防盗链

- **不同功能之间的域名独立开来**: 使用二级域名分类更清楚一点.
- **防止其他域名来的请求**

```
一般来说一个公司，他会有一个顶级域名，zhss.com，官网可以是www.zhss.com，你可能会有多个二级域名绑定到你的不同的系统上去，电商网站，www.zhss.com，第三方卖家，seller.zhss.com，把这个二级域名都跳转到商家系统，http://192.168.3.101:8080/seller$1 last，就可以把针对二级域名的请求都做一个跳转

一个公司可能会提供多个二级域名，多个二级域名都可以配置在nginx里，对应多个server，配置的跳转就是不同的二级域名的请求转发给不同的系统的服务器所在地址就可以了
```





### 110. 正向代理: 为client代理. 反向代理: 为server代理

反向代理+负载均衡



### 111. upstream指令配置后端服务器组

```lua
# 1. 配置一个服务器组: 默认轮询处理
upstream backend_servers {
	server backend1.zhss.com weight=5;	// 权重
	server backend2.zhss.com fail_timeout=30s; // 超时时间.
	server backend3.zhss.com;
}

server {
	listen 80;
    
    // 2. 把这个服务器组用作 `/` 路径
	server_name	www.zhss.com;
	location / {
		proxy_pass	backend_servers;
	}
}
```



### 112. upstream指定多个服务, 请求路径分组+组内负载均衡: 实现二级域名+负载均衡

```lua
upstream order_servers {
	server order1.zhss.com:8080;
	server order2.zhss.com:8080;
	server order3.zhss.com:8080;
}

upstream product_servers {
	server product1.zhss.com:8080;
	server product2.zhss.com:8080;
	server product3.zhss.com:8080;
}
# 这里就可以分组, 为每个服务负载均衡了
server {
	listen 80;
	server_name	www.zhss.com;
	index index.html;
	location /order/ {
		proxy_pass http://order_servers;
		proxy_set_header Host $host;
    }
    location /product/ {
        proxy_pass http://product_servers;
        proxy_set_header Host $host;
    }
}
## 拦截住二级域名
server {
	listen 80;
	server_name	platform.zhss.com;
	index index.html;
	location / {
		proxy_pass http://product_servers;
		proxy_set_header Host $host;
	}
}
```





### 114. Nginx之后+网关

在nginx里，不同的二级域名可以配置成不同的server，里面可以有不同的location，反向代理到不同的upstream服务器组做负载均衡



- **nginx如果直接配置具体服务器的地址: 需要频繁修改nginx配置**
- **加入gateway地址, 屏蔽了内部系统实现细节**



### 115. 大公司如何使用Nginx

- **LVS最前**
- **Nginx区分二级域名, 进行二级域名的负载均衡**
- **网关: 接收请求, 作为后台服务的入口, 动态感知服务变化, 负载均衡动态请求.**

 















































