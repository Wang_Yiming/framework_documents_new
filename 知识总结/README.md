

复习总结的 Release Node

#### [知识总结 v1.0 第一次跳槽](https://github.com/learnprogram2/knowledgePointReview)
20年2月之前为第一次跳槽准备总结的, 第一次跳槽比较傻, 知识点很多都不太理解, 最重要的是没有对自己的项目进行总结优化.
1. 明白了Java开发的工作内容: Spring单体项目+微服务框架(Dubbo)项目开发.
2. 开始了解Java多线程.
3. 简单的理解中间件, 但还不明白中间件的需求原因+实现+高性能高可用原理.
4. MySQL, Redis 简单理解和使用


#### [知识总结 v2.0 第二次跳槽](https://github.com/learnprogram2/tiao2021)
21年8月开始到9月底整理的, 现在还在进行. 还有很多没有学的, 很多遗憾. 但是不想在现在的公司里耗费青春了. 
继续, 等年底跳槽后整理一下自己最近两年学的大概内容.
1. 学会了看源码, 很好.
2. 学了一些架构设计要考虑的点.





