[447. 回旋镖的数量](https://leetcode-cn.com/problems/number-of-boomerangs/solution/an-zhao-gui-ze-bian-li-fa-by-wangyk-592f/)

给定平面上 n 对 互不相同 的点 points ，其中 points[i] = [xi, yi] 。回旋镖 是由点 (i, j, k) 表示的元组 ，其中 i 和 j 之间的距离和 i 和 k 之间的距离相等（需要考虑元组的顺序）。

返回平面上所有回旋镖的数量。

思路: 正常的按照规则遍历就好了.