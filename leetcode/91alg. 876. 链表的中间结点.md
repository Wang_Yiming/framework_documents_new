[876. 链表的中间结点](https://leetcode-cn.com/problems/middle-of-the-linked-list/solution/kuai-man-zhi-zhen-by-wangyk-5ws5/)

给定一个头结点为 head 的非空单链表，返回链表的中间结点。

如果有两个中间结点，则返回第二个中间结点。

 

思路: 快慢指针, 而且要注意如果是双数要返回第二个中间节点.




